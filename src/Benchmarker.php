<?php

namespace Benchmarker;

//test_Highmem
//test_PDO
//test_StringAdvanced -str_replace, number_format, sprint_f, str_pos, substr, implode, explode

// todo: levels (autocheck?) to make

class Benchmarker
{
    const LEVEL_DRYRUN = 0.001;
    const LEVEL_VERY_EASY = 0.1;
    const LEVEL_EASY = 0.5;
    const LEVEL_NORMAL = 1;
    const LEVEL_HARD = 2;
    const LEVEL_VERY_HARD = 5;
    const LEVEL_INSANE = 10;

    /**
     * @var array
     */
    private $methods;

    /**
     * @var array
     */
    private $exclude = array();

    /**
     * @var array
     */
    private $excluded_log = array();

    /**
     * @var array
     */
    private $general_log = array();

    public function __construct()
    {
        $this->methods      = preg_grep('/^test_/', get_class_methods($this));
        $this->method_inits = preg_grep('/^init_/', get_class_methods($this));

        asort($this->methods);

        $this->setLevel();
    }

    public function exclude_test($testname)
    {
        $this->exclude[] = $testname;
    }

    public function setLevel($level = self::LEVEL_NORMAL)
    {
        $this->level = $level;
    }

    private function log_excluded($method)
    {
        $this->excluded_log[] = $method;
    }

    private function log($log, $method = 'system')
    {
        $this->general_log[] = $method . ' : ' . $log;
    }

    private function pad_line($string, $line_length = 40, $outerpad = '', $line_ending = PHP_EOL)
    {
        $line_length -= (strlen($outerpad) * 2);

        return $outerpad . str_pad($string, $line_length, ' ', STR_PAD_BOTH) . $outerpad . $line_ending;
    }

    private function result_line($string, $result, $line_length = 40, $outerpad = '', $line_ending = PHP_EOL)
    {
        $result = number_format($result, 3);
        $to_pad = $line_length - strlen($outerpad) - 8;

        return $outerpad . str_pad($string, $to_pad) . $result . ' s' . $outerpad . $line_ending;
    }

    private function line($line_length = 40, $character = '-')
    {
        return str_repeat($character, $line_length) . PHP_EOL;
    }

    private function create_output_header()
    {
        $system  = (isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '') . (isset($_SERVER['SERVER_ADDR']) ? '@' . $_SERVER['SERVER_ADDR'] : '');
        $bitsize = PHP_INT_MAX > 2147483647 ? 64 : 32;

        $header = PHP_EOL . $this->line();
        $header .= $this->pad_line('BENCHMARKER', 40, '|');
        $header .= $this->pad_line('perfomance tester for php servers', 40, '|');
        $header .= $this->line();;
        $header .= 'Start : ' . date('Y-m-d H:i:s') . PHP_EOL;
        $header .= 'Server : ' . $system . PHP_EOL;

        if (!empty($_SERVER['NUMBER_OF_PROCESSORS'])) {
            $header .= 'CPUs : ' . (int)$_SERVER['NUMBER_OF_PROCESSORS'] . PHP_EOL;
        }

        $header .= 'Memory limit : ' . ini_get('memory_limit') . PHP_EOL;

        $header .= 'PHP version : ' . PHP_VERSION . PHP_EOL;
        $header .= 'Bits : ' . $bitsize . PHP_EOL;
        $header .= 'Platform : ' . PHP_OS . PHP_EOL;
        $header .= $this->line();;

        return $header;
    }

    public function run($toBrowser = false)
    {
        if ($this->level !== self::LEVEL_NORMAL) {
            $this->log('Intensity level modifier: ' . $this->level);
        }

        if ($this->isXDebugEnabled()) {
            $this->log('XDebug is enabled');
        }

        if ($this->isOpCacheEnabled($toBrowser)) {
            $this->log('OpCache is enabled');
        }

        if ($this->isWincacheEnabled()) {
            $this->log('Wincache is enabled');
        }

        $time_total = 0;
        $output     = $this->create_output_header();

        if (empty($this->methods)) {
            $output .= 'No tests specified';
        }

        foreach ($this->method_inits as $initMethod) {
            $this->$initMethod();
        }

        foreach ($this->methods as $method) {

            if (in_array($method, $this->exclude, true)) {
                $this->log_excluded($method);
                continue;
            }

            $time_total += $result = self::startTest($method, $this->level);
            $output     .= $this->result_line($method, $result, 40, ' ');
        }

        $this->log('Memory peak usage ' . $this->bytes_humanreadable(memory_get_peak_usage(true)));


        if (!empty($this->excluded_log)) {
            $output .= $this->line();
            $output .= 'EXCLUDED' . PHP_EOL . ' - ';
            $output .= implode(PHP_EOL . ' - ', $this->excluded_log) . PHP_EOL;
        }

        $output .= $this->line();
        $output .= $this->result_line('Total runtime', $time_total);

        if (!empty($this->general_log)) {
            $output .= $this->line(40, '=');
            $output .= 'LOG' . PHP_EOL;
            $output .= implode(PHP_EOL, $this->general_log) . PHP_EOL;
        }

        if ($toBrowser) {
            return '<pre>' . $output . '</pre>';
        }

        return $output;
    }

    private function isXDebugEnabled()
    {
        return extension_loaded('xdebug');
    }

    private function isOpCacheEnabled($toBrowser)
    {
        if (false === extension_loaded('Zend OPcache')) {
            return false;
        }

        if (false === (bool)ini_get('opcache.enable')) {
            return false;
        }

        if (false === $toBrowser) {
            return (bool)ini_get('opcache.enable_cli');
        }

        return true;
    }

    private function isWincacheEnabled()
    {
        if (false === extension_loaded('wincache')) {
            return false;
        }

        if (false === (bool)ini_get('wincache.fcenabled')) {
            return false;
        }

        return true;
    }

    private function bytes_humanreadable($bytes)
    {
        $i = (int)floor(log($bytes, 1024));

        return number_format($bytes / (1024 ** $i), [0, 1, 1, 2, 3][$i]) . ['B', 'kB', 'MB', 'GB', 'TB'][$i];
    }

    private static function startTest($method, $level = null)
    {
        $time_start = microtime(true);

        if (null !== $level) {

            $reflectionParameter = (new \ReflectionClass('Benchmarker\Benchmarker'))->getMethod($method)->getParameters();
            $runDefault          = $reflectionParameter[0]->getDefaultValue();

            $level = ceil($runDefault * $level);
        }

        self::$method($level);

        return number_format(microtime(true) - $time_start, 3);
    }


    protected function init_Math()
    {
        $mathFunctions = array('abs', 'acos', 'asin', 'atan', 'bindec', 'floor', 'ceil', 'round', 'exp', 'sin', 'tan', 'pi', 'is_finite', 'is_nan', 'sqrt');

        foreach ($mathFunctions as $key => $function) {
            if (false === function_exists($function)) {
                $this->log("'{$function}' does not exist", __FUNCTION__);
            }
        }
    }

    public static function test_Math($runs = 12500)
    {
        $mathFunctions = array('abs', 'acos', 'asin', 'atan', 'bindec', 'floor', 'ceil', 'round', 'exp', 'sin', 'tan', 'pi', 'is_finite', 'is_nan', 'sqrt');

        foreach ($mathFunctions as $key => $function) {
            if (false === function_exists($function)) {
                unset($mathFunctions[$key]);
            }
        }

        for ($i = 0; $i < $runs; $i++) {
            foreach ($mathFunctions as $function) {
                $r = call_user_func_array($function, array($i));
            }
        }
    }


    protected function init_StringBasic()
    {
        $stringFunctions = array('addslashes', 'chunk_split', 'strip_tags', 'strtoupper', 'strtolower', 'ucfirst', 'strrev', 'strlen', 'ord', 'urlencode');

        foreach ($stringFunctions as $key => $function) {
            if (false === function_exists($function)) {
                $this->log("'{$function}' does not exist", __FUNCTION__);
            }
        }
    }


    public static function test_StringBasic($runs = 20000)
    {
        $stringFunctions = array('addslashes', 'chunk_split', 'strip_tags', 'strtoupper', 'strtolower', 'ucfirst', 'strrev', 'strlen', 'ord', 'urlencode');

        foreach ($stringFunctions as $key => $function) {
            if (false === function_exists($function)) {
                unset($stringFunctions[$key]);
            }
        }

        $string = 'BENCHMARKER - perfomance tester for php servers - This is a string to be used as input parameter!';

        for ($i = 0; $i < $runs; $i++) {
            foreach ($stringFunctions as $function) {
                $r = call_user_func_array($function, array($string));
            }
        }
    }


    public static function test_Loops($runs = 4500000)
    {
        for ($i = 0; $i < $runs; ++$i) {
            //do nothing
        }
        $i = 0;
        while ($i < $runs) {
            ++$i;
        }
        $i = range(0, round($runs / 100));
        foreach ($i as $j) {
            //do nothing
        }
    }


    public static function test_IfElse($runs = 2000000)
    {
        for ($i = 0; $i < $runs; $i++) {
            if ($i === -1) {
            } elseif ($i === -2) {
            } elseif ($i === -3) {
            } else {
                if ($i === -4) {
                }
            }
        }
    }

    public static function test_Hash($runs = 200000)
    {
        $algos = array('sha256');

        $string = 'BENCHMARKER - perfomance tester for php servers - This is a string to be used as input parameter!';

        foreach ($algos as $algo) {
            for ($i = 0; $i < $runs; $i++) {
                $r = \hash($algo, $string, false);
            }
        }
    }

    public static function test_Base64($runs = 200000)
    {
        $string = 'BENCHMARKER - perfomance tester for php servers - This is a string to be used as input parameter!';

        for ($i = 0; $i < $runs; $i++) {
            $a = base64_encode($string);
            $b = base64_decode($a);
        }
    }

    public static function test_IO($runs = 1250)
    {
        $self = __FILE__;

        for ($i = 0; $i < $runs; $i++) {

            if (!file_exists($self)) {
                return;
            }

            $r = file_get_contents($self);
        }
    }

    public static function test_Json($runs = 100000)
    {
        $array = array('benchmarker' => array('performance', array('tester' => 3, 'for' => null, 'php' => 'servers')), 'testing' => 'an array');

        for ($i = 0; $i < $runs; $i++) {
            $a = json_encode($array);
            $b = json_decode($a);
        }
    }

    public static function test_DateTime($runs = 50000)
    {
        for ($i = 0; $i < $runs; $i++) {
            $a = new \DateTime();
            $b = new \DateInterval('P1D');
            $c = $a->add($b)->format('d-m-Y');
        }
    }

    protected function init_GD()
    {
        if (false === extension_loaded('gd')) {
            $this->exclude_test('test_GD');
            $this->log('the GD extension is not loaded');
        }
    }

    public static function test_GD($runs = 5000)
    {
        $string = 'BENCHMARKER - perfomance tester for php servers - This is a string to be used as input parameter!';

        for ($i = 0; $i < $runs; $i++) {
            $im = imagecreatetruecolor(200, 25);

            $text_color = imagecolorallocate($im, 233, 14, 91);
            imagestring($im, 1, 5, 5, $string, $text_color);
            imageflip($im, IMG_FLIP_VERTICAL);
            imagedestroy($im);
        }
    }

    public static function test_Curl($runs = 5)
    {
        for ($i = 0; $i < $runs; $i++) {
            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, "https://www.google.com/");
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 2);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //perhaps suggest this to user: https://snippets.webaware.com.au/howto/stop-turning-off-curlopt_ssl_verifypeer-and-fix-your-php-config/ + https://curl.haxx.se/docs/caextract.html

            $a = curl_exec($curl);
            curl_close($curl);
        }
    }


}
