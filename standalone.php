<?php

/*
 * Benchmarker - perfomance tester for php servers
 *
 * Benchmarker can be used with PSR-0/PSR-4 autolaading or as stand-alone script
 *
 * To use it as stand-alone copy this file and src/Benchmaker.php to any folder
 */

require_once __DIR__ . '/src/Benchmarker.php';

$benchmarker = new \Benchmarker\Benchmarker();
//$benchmarker->exclude_test('test_Curl');
//$benchmarker->setLevel(Benchmarker\Benchmarker::LEVEL_EASY);

echo $benchmarker->run(true);
